import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import Navigation from '@/constants/Navigation.js'

const store = new Vuex.Store({
  state: {
    constants: {
      navigation: Navigation
    },
    menu: {
      open: false
    }
  },
  getters: {
    getSections: (state) => {
      return state.sections;
    },
    getSection: (state) => {
      return state.constants[state.section];
    }
  },
  mutations: {
    toggleMenu: (state, payload) => {
      state.menu.open = payload;
    },
    toggleEmotion: (state, payload) => {
      let selected = (payload.selected) ? false : true;
      Vue.set(payload, 'selected', selected);
    }
  },
  actions: {}
});

export default store;
