const Navigation = [
  {
    text: 'Overview',
    to: '/',
    exact: true
  },
  {
    text: 'Send',
    to: '/send',
    exact: false
  },
  {
    text: 'Receive',
    to: '/receive',
    exact: false
  },
  {
    text: 'Transactions',
    to: '/transactions',
    exact: false
  },
  {
    text: 'Settings',
    to: '/settings',
    exact: false
  }
]

export default Navigation;
