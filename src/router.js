import Vue from 'vue'
import Router from 'vue-router'

import Overview from './views/Overview.vue'
import Receive from './views/Receive.vue'
import Send from './views/Send.vue'
import TransactionDetails from './views/TransactionDetails.vue'
import TransactionDetails01 from './views/TransactionDetails01.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Overview',
      component: Overview
    },
    {
      path: '/receive',
      name: 'Receive',
      component: Receive
    },
    {
      path: '/send',
      name: 'Send',
      component: Send
    },
    {
      path: '/transaction',
      name: 'TransactionDetails',
      component: TransactionDetails
    },
    {
      path: '/transaction01',
      name: 'TransactionDetails01',
      component: TransactionDetails01
    }
    /*{
      path: '/experience/:section?',
      name: 'Experience',
      component: Experience
    }*/
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // }
  ]
})
